import { Object3D, Vector3 } from "three";

export class VRControlsUtil {
    public static setWorldPose(transform: Object3D, worldPose: Vector3) {
        let pos = transform.parent.worldToLocal(worldPose.clone());
        transform.position.copy(pos);
        this.updateMatx(transform);
    }
    public static updateMatx(transform: Object3D) {
        transform.updateMatrix();
        transform.updateMatrixWorld(true);
        transform.updateWorldMatrix(true, false);
    }
}
