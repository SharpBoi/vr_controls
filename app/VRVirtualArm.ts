import { Object3D, Vector3, Matrix4, Euler, Plane } from "three";
import { Hand } from "./VRGamepadData";
import { VRControls } from "./VRControls";
import { Math as TMath } from "three"
import { VRControlsUtil } from "./VRControlsUtil";

/**
 * Virtual arm for 3dof gamepad headsets
 */
export class VRVirtualArm {
    public static GetArmBySide(arms: VRVirtualArm[], side: Hand) {
        for (let i = 0; i < arms.length; i++)
            if (arms[i].Side === side)
                return arms[i];
    }


    public ForearmLength = 0.5;
    public ForearmOffset = new Vector3(0.25, -0.7, 0);
    public Side = Hand.right;


    private forearm: Object3D;
    private neck: Object3D;


    public get Neck() { return this.neck }
    private get forearmEndWorld() { VRControlsUtil.updateMatx(this.forearm); return this.forearm.localToWorld(new Vector3(0, 0, -this.ForearmLength)) }
    private get forearmOffsetSided() { return this.ForearmOffset.clone().setX(this.ForearmOffset.x * (this.Side === Hand.right ? 1 : -1)) }


    constructor() {
        this.forearm = new Object3D();

        this.neck = new Object3D();
        this.neck.add(this.forearm);
    }


    /**
     * Update arm pose.
     * @param head object, represents player head. For example - main camera
     * @param gamepadObject gamepad transform to pose in virtual arm;
     */
    public UpdateArmPose(head: Object3D, gamepadObject: Object3D) {
        this.updateArmPose(head);
        this.updateGamepadPose(gamepadObject);
    }

    private updateArmPose(head: Object3D) {
        VRControlsUtil.updateMatx(head);
        VRControlsUtil.setWorldPose(this.neck, head.localToWorld(new Vector3));

        let vgd = VRControls.GetByHand(this.Side);
        this.neck.quaternion.copy(vgd.Quat);
        this.neck.quaternion.x = 0;
        this.neck.quaternion.z = 0;

        this.forearm.position.copy(this.forearmOffsetSided);
        VRControlsUtil.updateMatx(this.forearm);
    }
    private updateGamepadPose(gamepadObject: Object3D) {
        VRControlsUtil.setWorldPose(gamepadObject, this.forearmEndWorld);

        let vgd = VRControls.GetByHand(this.Side);
        this.forearm.quaternion.copy(vgd.Quat);
        this.forearm.quaternion.y = 0;
        this.forearm.quaternion.z = 0;
        gamepadObject.quaternion.copy(vgd.Quat);

        VRControlsUtil.updateMatx(gamepadObject);
        VRControlsUtil.updateMatx(this.forearm);
    }
}