import GLTFloader from "three-gltf-loader"
import { GLTF } from "./GLTF";
import { Object3D } from "three";
import { GamepadModelsNames } from "./GamepadModelsNames";
import { HMDsNames } from "./HMDsNames";
import { VRManager } from "./VRManager";
import Sivent from "sivent/lib";

export type Model_Device = { gamepadModelName: string, hmdName: string, model: Object3D }

/**
 * Utility for vr models loading and parse them 
 */
export class VRModelsLoader {
    private static loader = new GLTFloader();
    private static model_device_map: Model_Device[] = [
        { model: null, gamepadModelName: GamepadModelsNames.DayDream, hmdName: HMDsNames.LenovoDayDream },
        { model: null, gamepadModelName: GamepadModelsNames.GearVR, hmdName: HMDsNames.GearVR },
        { model: null, gamepadModelName: GamepadModelsNames.HTCVive, hmdName: HMDsNames.HTCVive },
        { model: null, gamepadModelName: GamepadModelsNames.OculusGo, hmdName: HMDsNames.OculusGo },
        { model: null, gamepadModelName: GamepadModelsNames.OculusQuest, hmdName: HMDsNames.OculusQuest },
        { model: null, gamepadModelName: GamepadModelsNames.OculusQuest, hmdName: HMDsNames.OculusRift },
        { model: null, gamepadModelName: GamepadModelsNames.WMR, hmdName: HMDsNames.WMR },
    ]


    // public static OnLoaded: (gltf: GLTF) => void = () => { };
    // public static OnProgress: (event: ProgressEvent<EventTarget>) => void = () => { };
    // public static OnError: (err: ErrorEvent) => void = () => { };

    public static OnLoaded = new Sivent<void, GLTF>();
    public static OnProgress = new Sivent<void, ProgressEvent<EventTarget>>();
    public static OnError = new Sivent<void, ErrorEvent>();

    private static _onLoaded = new Sivent<void, void>();

    /**
     * Path to folder, where gamepad.gltf located.
     * For example, if gamepad.gltf located in ```models/gamepad/gamepad.gltf```,  
     * then set ```models/gamepad```
     */
    public static ModelsGLTFUrl = "";
    /**
     * Name of gltf header files. Default is ```gamepad``` 
     */
    public static ModelHeaderName = "gamepad";
    /**
     * Extension of gltf header file. Default is ```gltf```
     */
    public static ModelHeaderExtension = "gltf";

    /** Basic gamepad Object3D with every gamepad model */
    public static SharedGamepadModel: Object3D = undefined;

    /** Does models loaded */
    public static ModelsLoaded = false;

    public static Init() {
        navigator.getVRDisplays().then(ds => {
            if (VRManager.Dispaly === undefined || VRManager.Dispaly === null)
                VRManager.SetDisplay(ds[0]);

            this._onLoaded.add((_, g) => {
                for (let i = 0; i < this.model_device_map.length; i++) {
                    if (VRManager.IsDevice(this.model_device_map[i].hmdName)) {
                        this.model_device_map[i].model.visible = true;
                        break;
                    }
                }
            });
        });
    }

    /**
     * Load gamepads models. Use it before vizualize your gamepads
     */
    public static Load() {
        this.ModelsLoaded = false;

        this.loader.load(`${this.ModelsGLTFUrl}/${this.ModelHeaderName}.${this.ModelHeaderExtension}`,
            (gltf) => {
                this.SharedGamepadModel = gltf.scene.children[0];

                this.SharedGamepadModel.traverse(c => {
                    for (let i = 0; i < this.model_device_map.length; i++) {
                        if (c.name === this.model_device_map[i].gamepadModelName) {
                            this.model_device_map[i].model = c;
                            c.visible = false;
                            c.position.x = 0;
                        }
                    }
                });

                this.ModelsLoaded = true;
                this._onLoaded.invoke();
                this.OnLoaded.invoke(null, gltf);
            },

            (e) => {
                this.OnProgress.invoke(null, e);
            },

            (e) => {
                this.OnError.invoke(null, e);
            });
    }

    /**
     * Get copy of SharedGamepadModel. Use to vizualize all your gamepads.  
     * Attention !!! Call it after enter vr mode, to get correct gamepad model.
     */
    public static GetCopyGamepadModel() {
        let clone = this.SharedGamepadModel.clone(true);
        return clone;
    }
}
