import { Object3D } from "three";
import Sivent from "sivent";
import { HMDsNames } from "./HMDsNames";
import { timingSafeEqual } from "crypto";
import { WebGLRenderer } from "three"

/**
 * Utility for enter/exit vr events, detect current vr mode, device detection
 */
export class VRManager {
    /** Fires, when we fall in vr mode. Data is VRDisplayEvent */
    public static onEnterVR = new Sivent<void, VRDisplayEvent>();
    /** Fires, when we out of vr mode. Data is VRDisplayEvent */
    public static onExitVR = new Sivent<void, VRDisplayEvent>();

    private static isVrNow = false;
    private static display: VRDisplay = undefined;


    /**
     * Are we in vr now
     */
    public static get IsVRNow() { return this.isVrNow }
    /**
     * VR display. Valid in vr mode
     */
    public static get Dispaly() { return this.display }


    /**
     * Initialize workflow of VRManager
     */
    public static Init(renderer: WebGLRenderer) {
        navigator.getVRDisplays().then(ds => {
            this.display = ds[0];
        });

        window.addEventListener("vrdisplaypresentchange", (ev) => {
            let vev = ev as VRDisplayEvent;

            this.display = vev.display;
            this.isVrNow = this.display.isPresenting;

            if (this.display.isPresenting === false) {
                this.onExitVR.invoke(null, vev);
                renderer.vr.enabled = false;
            }
            if (this.display.isPresenting) {
                this.onEnterVR.invoke(null, vev);
                renderer.vr.enabled = true;
            }

        });
    }

    public static SetDisplay(disp: VRDisplay) {
        this.display = disp;
    }
    public static IsDevice(deviceName: string) {
        // return this.display.displayName.toLowerCase() === deviceName.toLowerCase();
        return this.display.displayName.toLowerCase().indexOf(deviceName.toLowerCase()) !== -1;
    }
}
