import { VRVirtualArm } from "./VRVirtualArm";
import { Object3D, Vector3, Matrix4, Scene } from "three";
import { VRControls } from "./VRControls";
import { VRGamepadData, DOF, Hand } from "./VRGamepadData";
import { VRModelsLoader } from "./VRModelsLoader";
import { VRManager } from "./VRManager";
import { VRControlsUtil } from "./VRControlsUtil";

/**
 * Default implementation of vr body. Automatize posing of 3dof and 6dof controllers, select gamepads models, vizualization
 */
export class VRBodyDefault {

    private arms: VRVirtualArm[] = [];
    private vgds: VRGamepadData[] = [];
    private models: Object3D[] = [];
    private hands: Object3D[] = [];


    /** Containers for gamepad. Where [0] - leftmost, [1] - righmost.  
     * By default its a just a Object3D, but u can replace them it with your custom objects 
     */
    public get Hands() { return this.hands }


    constructor() {
        // arms
        let leftArm = new VRVirtualArm();
        leftArm.Side = Hand.left;

        let rightArm = new VRVirtualArm();
        rightArm.Side = Hand.right;

        let touchpad = new VRVirtualArm();
        touchpad.Side = Hand.touchpad;

        this.arms = [leftArm, rightArm, touchpad];


        // hands
        this.hands = [new Object3D(), new Object3D(), new Object3D()];


        if (VRModelsLoader.ModelsLoaded) {
            this.setHandModels();
        }
        else {
            VRModelsLoader.OnLoaded.add((_, g) => {
                this.setHandModels();
            });
            VRModelsLoader.Load();
        }
    }

    private setHandModels() {
        VRManager.onEnterVR.add((_, ev) => {
            if (this.models.length === 0) {
                for (let i = 0; i < 3; i++) {
                    this.models[i] = VRModelsLoader.GetCopyGamepadModel();
                    this.hands[i].add(this.models[i]);
                    this.models[i].position.set(0, 0, 0);
                    this.models[i].rotation.set(0, 0, 0);
                }
                this.models[2].scale.set(0, 0, 0);
            }
        })
    }

    /**
     * Replace hand object with new object. Moves children from prev to new
     * @param handIndex index of hand, that shoud be replaced
     * @param newObject new hand object
     */
    public ReplaceHand(handIndex: number, newObject: Object3D) {
        for (let i = 0; i < this.hands[handIndex].children.length; i++) {
            newObject.add(this.hands[handIndex].children[i]);
        }
        this.hands[handIndex] = newObject;
    }

    /**
     * Update gamepads data
     */
    public UpdateGamepadsData(standMatx: Matrix4) {
        VRControls.Update(standMatx);
        this.vgds = VRControls.Gamepads;
    }

    /**
     * Update pose of vr body and gamepads
     * @param scene world scene
     * @param head head representation object. Usually its a main camera
     * @param standMatx standing matrix from YourRenderer.vr.getStandingMatrix()
     */
    public UpdatePose(scene: Scene, head: Object3D) {
        let headParentWorld = new Vector3();
        VRControlsUtil.updateMatx(head.parent);
        head.parent.localToWorld(headParentWorld);

        // hierarch hands
        for (let i = 0; i < this.hands.length; i++) {
            if (this.hands[i].parent !== scene)
                scene.add(this.hands[i]);
        }

        // pose based on controller dof
        for (let i = 0; i < this.vgds.length; i++) {
            let vgd = this.vgds[i];

            // use virtual arms
            if (vgd.Dof === DOF.dof3) {
                let arm = VRVirtualArm.GetArmBySide(this.arms, vgd.Hand);
                if (arm.Neck.parent !== scene) scene.add(arm.Neck);

                if (vgd.Hand === Hand.left)
                    arm.UpdateArmPose(head, this.hands[0]);
                else if (vgd.Hand === Hand.right)
                    arm.UpdateArmPose(head, this.hands[1]);
                else if (vgd.Hand === Hand.touchpad)
                    arm.UpdateArmPose(head, this.hands[2]);
            }
            // use position
            else if (vgd.Dof === DOF.dof6) {
                if (vgd.Hand === Hand.left) {
                    VRControlsUtil.setWorldPose(this.hands[0], vgd.Pos.clone().add(headParentWorld));
                    this.hands[0].quaternion.copy(vgd.Quat);
                    VRControlsUtil.updateMatx(this.hands[0]);
                }
                else if (vgd.Hand === Hand.right) {
                    VRControlsUtil.setWorldPose(this.hands[1], vgd.Pos.clone().add(headParentWorld));
                    this.hands[1].quaternion.copy(vgd.Quat);
                    VRControlsUtil.updateMatx(this.hands[1]);
                }
                else if (vgd.Hand === Hand.touchpad) {
                    VRControlsUtil.setWorldPose(this.hands[2], vgd.Pos.clone().add(headParentWorld));
                    this.hands[2].quaternion.copy(vgd.Quat);
                    VRControlsUtil.updateMatx(this.hands[2]);
                }
            }
        }
    }
}
