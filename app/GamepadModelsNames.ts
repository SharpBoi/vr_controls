export const GamepadModelsNames = {
    DayDream: "gamepad-DayDream",
    GearVR: "gamepad-GearVR",
    OculusGo: "gamepad-OculusGo",
    OculusQuest: "gamepad-OculusQuest",
    HTCVive: "gamepad-HTCVive",
    WMR: "gamepad-wmr"
}