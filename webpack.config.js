const path = require('path');
const nodeExternals = require("webpack-node-externals");

const dist = "lib";

module.exports = {
    mode: 'production',
    entry: path.join(__dirname, 'app', 'index'),
    watch: false,
    output: {
        path: path.join(__dirname, dist),
        publicPath: `/${dist}/`,
        filename: "index.js",
        chunkFilename: '[name].js'
    },

    target: "node",
    externals: [nodeExternals()],

    module: {
        rules: [
            {
                test: /.jsx?$/,
                include: [
                    path.resolve(__dirname, 'app')
                ],
                exclude: [
                    path.resolve(__dirname, 'node_modules')
                ],
                loader: 'babel-loader',
                query: {
                    presets: [
                        ["@babel/env", {
                            "targets": {
                                "browsers": "last 2 chrome versions"
                            }
                        }]
                    ]
                }
            },

            {
                test: /.tsx?$/,
                include: [
                    path.resolve(__dirname, 'app')
                ],
                exclude: [
                    path.resolve(__dirname, 'node_modules')
                ],
                loader: 'ts-loader',
            }
        ]
    },
    resolve: {
        extensions: ['.json', '.js', '.jsx', '.ts']
    },
    devServer: {
        contentBase: path.join(`/${dist}/`),
        inline: true,
        host: '0.0.0.0',
        port: 8080,
    },

    optimization: {
        minimize: false
    }
};