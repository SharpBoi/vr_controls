# WebVR input utility

## About
This package utilize loading, selection gamepads models for current vr device, positioning them relative head.  
Typescript ready.
### Supported devices:
1. Lenovo DayDream
2. Oculus Rift
3. Oculus Go
4. Oculus Quest
5. Gear VR
6. HTC Vive
7. WinowsMixedReality(WIP)

## Installation
1. Install package:  `npm i vr_controls`  

2. Include models in your bundle:  
    1. If you use `webpack`, you should copy models directory somewhere in your destination directory.  
        It is better to use `custom copy plugin`, instead of `CopyWebpackPlugin`, because, the 2nd one has some problems when copying gltf`s .bin file.
        ```javascript
        ...
        plugins: [
            ...
            {
                apply: (cmplr) => {
                    cmplr.hooks.afterEmit.tap("MyCopyPlug", (cmpln) => {
                        let from = path.join(__dirname, "node_modules", "vr_controls", "lib", "gamepad");
                        let to = path.join(__dirname, "dist", "models", "gamepad");

                        let files = fs.readdirSync(from);
                        for (let i = 0; i < files.length; i++) {
                            fs.copyFileSync(
                                path.join(from, files[i]),
                                path.join(to, files[i]));
                        }

                        console.log("custom copy applied");
                    })
                }
            },
            ...
        ]
        ```
        In this case, files from `gamepad` folder will be copied into `__dirname/dist/models/gamepad`, where `dist` -- is my destination folder.

## Members
1. `VRModelsLoader` -- Utility for vr models loading and parse them.  
2. `VRControls` -- Gets native gamepads and serialzie its into handy wrappers
3. `VRGamepadData` -- Handy native gamepad data wrapper, serialize data into three.js types
4. `VRManager` -- Utility for enter/exit vr events, detect current vr mode, current device detection
5. `VRBodyDefault` -- basic implementation of vr body, that automatically handle 3dof and 6 dof gamepads. If there is 3dof gamepads -- Body will pose them with virtual arm. If 6dof - will pose them as it is.
6. `GamepadModelsNames` -- dictionary for gamepads models names.
7. `HMDsNames` -- dictionary for known head mounted devices.


## Usage
You can use package in `default` and `custom`  way.   
In any case, first of all, you should initialize VRManager, and pass the gamepad model path (in dist folder) into `VRModelsLoader`.   
```typescript
VRManager.Init(YourRenderer);
VRModelsLoader.ModelsGLTFUrl = "models/gamepad";
...
```
In the example above, this path specified, because `webpack` will copy models in this(`models/gamepad`) folder, and my dist looks like this:

![](https://gitlab.com/SharpBoi/vr_controls/raw/master/mdimgs/dist.PNG)
---
### Usage examples  

1. Example of default way:  
    `Body` will automatically pose gamepads, depends on their DOF.
	```typescript
	
	let body = new VRBodyDefault();

	yourUpdateLoop(() => {
	    body.UpdateGamepadsData(Renderer.vr.getStandingMatrix());
	    body.UpdatePose(MainScene, HeadObject);
	});
	```
	The `body.Hands[]` contains instances of Object3D, that used as posing points. Gamepads 3d models snaps on them.  
    It is safe, to replace them with `body.ReplaceHand(...)`, on your own objects at the run time.  
    `Hands[0]` - left hand; `Hands[1]` - right hand

2. Custom way:  
	...description in progress
